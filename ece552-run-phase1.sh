#!/bin/sh

echo "******************************************************"
echo "Running asm tests..."
echo "******************************************************"

wsrun.pl -list list_simple.list proc_hier_bench *.v 
cp summary.log ./../test_sum/instTests.summary.log

wsrun.pl -list list_complex.list proc_hier_bench *.v
cp summary.log ./../test_sum/complex_demo1.summary.log

wsrun.pl -list list_rand_simple.list proc_hier_bench *.v
cp summary.log ./../test_sum/rand_simple.summary.log

wsrun.pl -list list_rand_complex.list proc_hier_bench *.v
cp summary.log ./../test_sum/rand_complex.summary.log

wsrun.pl -list list_rand_ctrl.list proc_hier_bench *.v
cp summary.log ./../test_sum/rand_ctrl.summary.log

wsrun.pl -list list_rand_mem.list proc_hier_bench *.v
cp summary.log ./../test_sum/rand_mem.summary.log

echo "******************************************************"
echo "Printing failed tests..."
echo "******************************************************"
echo 

echo "instTests.summary.log"
grep 'FAILED' ./../test_sum/instTests.summary.log
echo

echo "complex_demo1.summary.log"
grep 'FAILED' ./../test_sum/complex_demo1.summary.log
echo

echo "rand_simple.summary.log"
grep 'FAILED' ./../test_sum/rand_simple.summary.log
echo

echo "rand_complex.summary.log"
grep 'FAILED' ./../test_sum/rand_complex.summary.log
echo 

echo "rand_ctrl.summary.log"
grep 'FAILED' ./../test_sum/rand_ctrl.summary.log
echo 

echo "rand_mem.summary.log"
grep 'FAILED' ./../test_sum/rand_mem.summary.log
echo 

echo "******************************************************"
echo "Running name check....."
echo "******************************************************"
name-convention-check >> ./../test_sum/name-convention-check.log

echo "******************************************************"
echo "Running vcheck-all....."
echo "******************************************************"
vcheck-all.sh >> ./../test_sum/vcheck.log

echo "******************************************************"
echo "All done!....."
echo "******************************************************"
