/******************************************************************************
 * File Name    - flash_boot.c
 * 
 * Description  - This c file contains the function definitions of all the 
 *                functions used by the flash boot code. The flash boot 
 *                validates the user application and launches the application 
 *                if the validation is successful.
 ******************************************************************************/

#include <stdlib.h>
#include "secure_rom_boot.h"

void* flash_boot_end_ptr = FLASH_END_ADDR_MEM;
void* flash_boot_version_ptr = &flash_boot_curr_version;

// ToDo: Cant be a variable. Should be a memory location that is preserved on boot
uint8_t flash_boot_curr_version = 0.0;

boot_result_t rom_boot_invalidate_checks()
{
    uint8_t* magic_number_ptr = FLASH_MAGIC_NUMBER_ADDR;
    uint8_t* valid_flag_ptr = FLASH_BOOT_IS_VALID_ADDR;

    // ToDo: Figure out how flash writes are done in ROM
    *magic_numbere_ptr = 0;
    *valid_flag_ptr = FLASH_BOOT_NOT_VALID;

    // If flash write is success 
    return BOOT_SUCCESS;
}

boot_result_t rom_boot_valid_success()
{
    uint8_t* magic_number_ptr = FLASH_MAGIC_NUMBER_ADDR;
    uint8_t* valid_flag_ptr = FLASH_BOOT_IS_VALID_ADDR;

    // ToDo: Figure out how flash writes are done in ROM
    *magic_numbere_ptr = FLASH_MAGIC_NUMBER;
    *valid_flag_ptr = FLASH_BOOT_VALID;

    // If flash write is success 
    return BOOT_SUCCESS;
}

boot_result_t rom_boot_validate_flash_boot()
{

}

boot_result_t rom_boot_validate_version()
{
    uint8_t* version_ptr = FLASH_BOOT_VERSION_ADDR;

    if(*version_ptr > *flash_boot_version_ptr)
    {
        *version_ptr = flash_boot_version_ptr;
        return BOOT_SUCCESS;
    }
    else if(*version_ptr == *flash_boot_end_ptr)
    {
        return BOOT_SUCCESS;
    }
    else
    {
        return BOOT_FLASH_VERSION_ERROR;
    }
}

boot_result_t rom_boot_validate_flash_boot()
{
    uint8_t* magic_number_ptr = FLASH_MAGIC_NUMBER_ADDR;
    uint8_t* valid_flag_ptr = FLASH_BOOT_IS_VALID_ADDR;

    if(*magic_number_ptr == FLASH_MAGIC_NUMBER && *valid_flag_ptr == FLASH_BOOT_VALID)
    {
        flash_boot();

        // Should not return;

        while(1);

        return BOOT_SUCCESS;
    }
    else
    {
        return BOOT_FLASH_BOOT_CORRUPT;
    }
}