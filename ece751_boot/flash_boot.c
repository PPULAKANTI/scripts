/******************************************************************************
 * File Name    - flash_boot.c
 * 
 * Description  - This c file contains the function definitions of all the 
 *                functions used by the flash boot code. The flash boot 
 *                validates the user application and launches the application 
 *                if the validation is successful.
 ******************************************************************************/